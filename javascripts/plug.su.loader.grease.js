// ==UserScript==
// @name        Sound Universe Bot Loader for Tampermonkey (Firefox)
// @namespace   http://www.xuniver.se/
// @description Simple PlugDJ bot loader.
// @include     https://plug.dj/*
// @version     1.0
// @grant       none
// ==/UserScript==

var SuBotURL = 'https://ENTER-YOUR-DOMAIN/plug.su-bot/javascripts/';

window.addEventListener('DOMContentLoaded',function(e)
{
	setTimeout(function(){
	
		var script;
		
		script = document.createElement('script');
		script.src = SuBotURL + 'plug.su.lang.js';
		document.body.appendChild(script);
		
		script = document.createElement('script');
		script.src = SuBotURL + 'plug.su.js';
		document.body.appendChild(script);
		
	},5000);
});
