
var SuPlugDJRoom 		= 'Sound Universe';
var SuPlugDJRoomSlug 	= 'sound-universe';
var SuPlugBotDomain 	= 'ENTER-YOUR-DOMAIN'; //Domain for chatter
var SuPlugBotToken 		= 'ENTER_YOUR_KEY'; //Key for chatter

function SoundUniverseBotChatter(bot,domain,token)
{
	this.bot 		= bot;
	this.xmlhttp	= new XMLHttpRequest();
	
	this.url 		= 'https://' + domain + '/plug.su-bot/index.php?message=%text%&token=%key%';
	this.token		= token;
	
	this.username 	= '';
	this.timer;
}

SoundUniverseBotChatter.prototype = {
	get:function(url)
	{
		this._content();
		this.xmlhttp.open('GET',url,true);
		this.xmlhttp.send(null);
	},
	
	_content:function()
	{
		var self = this;
		this.xmlhttp.onreadystatechange = function()
		{
			if (self.xmlhttp.readyState == 4 && self.xmlhttp.status == 200)
			{
				self.bot._sendChat('@' + self.username + ' ' + self.xmlhttp.responseText);
				self.username = '';
			}
		}
	}
};

function SoundUniverseBot(room)
{
	this.room 		= room;
	this.author 	= 'Josh aka XzaR';
	this.version 	= '1.872';
	this.url 		= 'http://www.xuniver.se';
	
	this.maximumTrackLength		= 600;
	this.lengthBeforeSkip 		= 180;
	
	this.sKey 					= '';
	this.sKeyUsers 				= '';
	this.sKeyStatistics 		= '';
	this.sKeyVars 				= '';
	
	this.chatter 				= null;
	this.chatBuffer 			= [];
	
	this.negative 				= false;
	
	this.langBot 				= 'en';
	this.lang 					= {};
	this.translator				= {};
	
	this.bot					= {};
	this.users 					= {};
	this.usersOnline 			= {};
	this.usersPlugDJ			= {};
	
	this.score					= {};
	
	this.statistics	= {
						joins			: 1,
						woots			: 0,
						mehs			: 0,
						curates			: 0,
						songs			: 0,
						skips			: 0,
						registeredDate  : ""
	};
	
	this.vars = {reloaded:false,lastAutoMessage:0};
	
	this.timerChat;
	this.timerBot;
	this.timerAutoMsg;
	this.timerVote;
	this.timerSkip;
	
	this.staffNames					= [];
	this.commandNames				= {};
	this.commandManuals				= {};
	this.autoMessages				= {};
}

SoundUniverseBot.prototype = {

	init:function(){
	
		if(typeof SoundUniverseBotLang == 'undefined')
		{
			window.location.reload();
			return false;
		}
		
		SoundUniverseBotLang.call(this);
	
		var self = this;
		
		this.sKey 				= 'subot_' + this.room + '_';
		this.sKeyUsers 			= this.sKey + 'users';
		this.sKeyStatistics 	= this.sKey + 'statistics';
		this.sKeyVars 			= this.sKey + 'vars';

		var users = window.localStorage.getItem(this.sKeyUsers);
		if(users){
		
			this.users = JSON.parse(users);
		}
		
		var statistics = window.localStorage.getItem(this.sKeyStatistics);
		if(statistics){
		
			this.statistics = JSON.parse(statistics);
		}
		
		var vars = window.localStorage.getItem(this.sKeyVars);
		if(vars){
		
			this.vars = JSON.parse(vars);
		}

		self.vote();
		this.timerVote = window.setInterval(function(){
		
			self.vote();
			
		},1000*20);
		
		var findMedia = window.setInterval(function(){
		
			var playback = document.getElementById('playback');
			if(playback) playback.innerHTML = '';
			
			var audience = document.getElementById('audience');
			if(audience) audience.innerHTML = '';
			
			if(audience && playback)
			{
				clearInterval(findMedia);
				self.updateUsers();
			}
			
		},1000*10);
		
		var iLoaderTries = 0;
		window.setInterval(function(){
		
			var disconnect 	= document.getElementsByClassName('disconnect')[0];
			var dialog = document.getElementsByClassName('dialog-frame')[0];
			
			var roomLoader = document.getElementById('room-loader');
			if(roomLoader) iLoaderTries++;
			else iLoaderTries = 0;
			
			if(disconnect || dialog || 
				document.title.indexOf("error") != -1 || 
				document.title.indexOf("connecting") != -1 || 
				iLoaderTries > 4)
				
				window.location.reload();
			
		},1000*15);
		
		var str = this.translate(this.langBot,'C_VERSION');
		str 	= str.replace('%version%',this.version);
		
		return true;
	},
	
	load:function(chatter){
	
		var self = this;

		this.chatter = chatter;
		this.setBot();
		this.updateUsers();
		
		API.setVolume(0);
		
		if(this.timerAutoMsg)
			window.clearInterval(this.timerAutoMsg);
	
		this.timerAutoMsg = window.setInterval(function(){
			
			self.printAutoMessage(self.langBot);
			
		},1000*60*5);
		
		var self = this;
		setTimeout(function(){ 
		
			var dj = API.getDJ();
			if(typeof dj != 'undefined'){
			
				var media = API.getMedia();
				self._tooLong(media.duration,dj.username);
				self._thinkMeh();
			}
			
			if(self.vars.reloaded)
			{
				var str = self.translate(self.langBot,'BOT_RELOADED');
				self._sendChat(str);
				self.setVar('reloaded',false);
			}
			
		},4000);
		
		console.log(this.translate(this.langBot,'C_LOADED'));
	},
	
	printAutoMessage:function(lang)
	{
		var msgLength = this.autoMessages[lang].length;
		var msgIndex = this.vars.lastAutoMessage || 0;
		var str = this.translateAMessages(lang,msgIndex);
		switch(msgIndex)
		{
			case 0:
				str = str.replace('%botname%',this.getBot('username'));
				break;
		}
		
		msgIndex++;
		if(msgIndex > msgLength-1)
			msgIndex = 0;
			
		this.setVar('lastAutoMessage',msgIndex);
		
		this._sendChat(str);
	},
	
	_thinkMeh:function(score){
		
		var score = score || API.getScore();
		var total = this.score.negative + this.score.positive;
		
		if(this.score.negative > 3 && this.score.positive < 4)
		{
			this.negative = true;
		}
		
		if(total > (Object.keys(this.usersPlugDJ).length / 2) && this.score.negative >= Math.ceil(total*0.80))
		{
			API.moderateForceSkip();
		}
	},
	
	_translate:function(code,key,type){
	
		var lang = {};
		switch(type)
		{
			case 1: 
				lang = this.commandNames;
				break;
			case 2: 
				lang = this.commandManuals;
				break;
			case 3: 
				lang = this.autoMessages;
				break;
			default: 
				lang = this.lang;
				break;
		}
	
		if(code && typeof lang[code] != 'undefined')
			return lang[code][key];
		
		return lang['en'][key];
	},
	
	setVar:function(key,value){
	
		this.vars[key] = value;
		window.localStorage.setItem(this.sKeyVars, JSON.stringify(this.vars));
	},
	
	translate:function(code,key){
	
		return this._translate(code,key,0);
	},
	
	translateAMessages:function(code,key){
	
		return this._translate(code,key,3);
	},
	
	translateCommand:function(code,key,syntax){
	
		if(syntax)
			return '!' + this._translate(code,key,1);
			
		return this._translate(code,key,1);
	},
	
	translateManual:function(code,key){
	
		return this._translate(code,key,2);
	},
	
	setBot:function(){
	
		var bot = API.getUser();
		this.bot = {
			id:bot.id,
			username:bot.username
		};
	},
	
	setOnlineUser:function(user){
	
		this.usersOnline[user.id] = {
			username	:user.username,
			curated		:user.curated,
			vote		:user.vote
		};
	},
	
	setPlugDJUser:function(user,id){
	
		var uid = id;
		var name = 'unknown';
		var lang = 'en';
		
		if(Object.keys(user).length > 0)
		{
			uid = user.id
			name = user.username;
			
			if(typeof user.language != 'undefined')
				lang = user.language;
		}
		
		if(uid)
		{
			if(typeof this.users[uid] != 'undefined')
			{
				if(this.users[uid].language)
					lang = this.users[uid].language;
			}
		
			this.usersPlugDJ[uid] = {
				username:name,
				language:lang
			};
		}
	},
	
	setNumber:function(num){
		if(isNaN(num) === false)
			return num;
			
		return 0;
	},
	
	updateUsers:function(){
	
		var users = API.getUsers();
		for(var index in users)
		{
			this.setPlugDJUser(users[index]);
			if(typeof this.users[users[index].id] != 'undefined'){
				
				this.setOnlineUser(users);
			}
		}
	},
	
	getPDJUser:function(id,key) {
	
		if(typeof this.usersPlugDJ[id] == 'undefined' || !this.usersPlugDJ[id][key]){
			var user = API.getUser(id);
			this.setPlugDJUser(user,id);
		}
		
		try
		{
			return this.usersPlugDJ[id][key];
		}
		catch(e)
		{
			return {username:'unknown',language:'en'};
		}
	},
	
	getBot:function(key) {
	
		if(typeof this.bot[key] == 'undefined'){
			this.setBot();
		}
	
		return this.bot[key];
	},
	
	saveStats:function(){
		
		window.localStorage.setItem(this.sKeyStatistics, JSON.stringify(this.statistics));
	},
	
	saveUsers:function(){
	
		window.localStorage.setItem(this.sKeyUsers, JSON.stringify(this.users));
	},
	
	vote:function(){
	
		var self = this;
		window.setTimeout(function(){

			if(self.negative) self.meh();
			else self.woot();
			
		},1000*(Math.random()*5+5));
	},
	
	woot:function(){
	
		var woot = document.getElementsByClassName('icon-woot')[0];
		if(woot)
			woot.click();
	},
	
	meh:function(){

		var meh = document.getElementsByClassName('icon-meh')[0];
		if(meh)
			meh.click();
	},
	
	_getArray:function(object,key){
	
		var key = (key) ? true : false;
		var commands = [];
		for(var index in object)
		{
			if(key){
			
				commands.push(index);
				continue;
			}
			
			commands.push(object[index]);
		}
		
		return commands;
	},
	
	_getArrayPart:function(object,page,max,key){
	
		var arr = this._getArray(object,key);
		
		var maxPage = Math.ceil(arr.length/max);
		if(page > maxPage)
			page = maxPage;
		
		var end = max*page;
		
		return [arr.slice(max*(page - 1),end),page,maxPage];
	},
	
	getCommands:function(lang){
	
		var code = 'en';
		if(typeof lang != 'undefined')
			code = lang;
		
		if(typeof this.commandNames[code] != 'undefined')
			return this.commandNames[code];
		
		return this.commandNames['en'];
	},
	
	sprintCommands:function(lang,page,max){

		var page = 	page 	|| 1;
		var max = 	max 	|| 7;
		
		var commands = this._getArrayPart(this.getCommands(lang),page,max);
		
		var str = '!' + commands[0].join(', !');
		if(commands[1] < commands[2])
			str += ', !' + this.translateCommand(lang,'COMMANDS') + ' ' + (commands[1] + 1);
		
		return str;
	},
	
	sprintLanguages:function(lang,page,max){

		var page = 	page 	|| 1;
		var max = 	max 	|| 10;
		
		var languages = this._getArrayPart(this.lang,page,max,true);
		
		var str = languages[0].join(', ');
		if(languages[1] < languages[2])
			str += ', !' + this.translateCommand(lang,'LANGUAGE_LIST') + ' ' + (languages[1] + 1);
		
		return str;
	},
	
	sprintUserStats:function(lang,id,target,page){
	
		var str = "";
		var str_target = this.translate(lang,'USERSTATS_SELF');
		if(target != null)
		{
			str_target = this.translate(lang,'USERSTATS_TARGET');
			str_target = str_target.replace('%target%',target);
		}
		
		switch(page)
		{
			case 2:
				str = this.translate(lang,'USERSTATS2');
				str = str.replace('%target%',	str_target);
				str = str.replace('%joins%',	this.users[id].joins);
				str = str.replace('%votes%',	this.users[id].woots + this.users[id].mehs);
				str = str.replace('%songs%',	this.users[id].songs);
				str = str.replace('%skips%',	this.users[id].skips);
				break;
			default:
				str = this.translate(lang,'USERSTATS');
				str = str.replace('%target%',	str_target);
				str = str.replace('%woots%',	this.users[id].woots);
				str = str.replace('%mehs%',		this.users[id].mehs);
				str = str.replace('%curates%',	this.users[id].curates);
				break;
		}
		
		return str;
	},
	
	sprintStats:function(lang,page){
	
		var str = "";
		switch(page)
		{
			case 2:
				str = this.translate(lang,'STATS2');
				str = str.replace('%skips%',	this.statistics.skips);
				str = str.replace('%songs%',	this.statistics.songs);
				str = str.replace('%joins%',	this.statistics.joins);
				str = str.replace('%users%',	Object.keys(this.users).length);
				break;
			default:
				str = this.translate(lang,'STATS');
				str = str.replace('%woots%',	this.statistics.woots);
				str = str.replace('%mehs%',		this.statistics.mehs);
				str = str.replace('%votes%',	this.statistics.woots + this.statistics.mehs);
				str = str.replace('%curates%',	this.statistics.curates);
				break;
		}
	
		return str;
	},

	removeUser:function(user){
	
		delete this.users[user.id];
		delete this.usersOnline[user.id];
		
		this.saveUsers();
		
		if(user.role == API.ROLE.DJ)
			API.moderateSetRole(user.id, API.ROLE.NONE);
	},
	
	removeFirstUser:function(){
	
		var firstKey = Object.keys(this.users)[0];
	},
	
	_sendChat:function(str){
		
		if(str)
			this.chatBuffer.push(str);
		
		if(this.timerChat) 
			clearTimeout(this.timerChat);
		
		var self = this;
		this.timerChat = setTimeout(function()
		{
			var message = self.chatBuffer.shift();
			API.sendChat(message);
		
			if(self.chatBuffer.length > 0) self._sendChat();
		
		},650);
	},
	
	_userJoin:function(user){
		
		var str = this.translate(this.langBot,'JOINED');
		str = str.replace('%username%',user.username);
		this._sendChat(str);
		
		this.setPlugDJUser(user);
		if(typeof this.users[user.id] != 'undefined'){
		
			this.setOnlineUser(user);
			
			this.users[user.id].username = user.username;
			this.users[user.id].joins++;
			this.saveUsers();
		}
		
		var self = this;
		setTimeout(function(){
		
			str = self.translate(self.getPDJUser(user.id,'language'),'WELCOME');
			str = str.replace('%username%',user.username);
			str = str.replace('%room%',self.room);
			str = str.replace('%botname%',self.getBot('username'));
			self._sendChat(str)
		},500);
		
		this.statistics.joins++;
		this.saveStats();
	},
	
	_userLeave:function(user){
		
		var str = this.translate(this.langBot,'LEFT');
		str = str.replace('%username%',user.username);
		this._sendChat(str);
		
		if(typeof this.usersPlugDJ[user.id] != 'undefined')
		{
			delete this.usersPlugDJ[user.id];
		}
		
		if(typeof this.usersOnline[user.id] != 'undefined')
		{
			delete this.usersOnline[user.id];
		}
	},
	
	_userCurate:function(obj){
	
		if(typeof this.usersOnline[obj.user.id] == 'undefined')
		{
			this.setOnlineUser(obj.user);
			this.usersOnline[obj.user.id].curated = false;
		}
	
		if(!this.usersOnline[obj.user.id].curated)
		{
			var media = API.getMedia();
		
			var str = this.translate(this.langBot,'CURATED');
			str = str.replace('%username%',obj.user.username);
			str = str.replace('%trackauthor%', media.author);
			str = str.replace('%tracktitle%',media.title);
			this._sendChat(str);
			
			this.usersOnline[obj.user.id].curated = true;
		}
	},
	
	_userSkip:function(user){
	
		for(var index in this.users)
		{
			if(this.users[index].username == user)
			{
				this.users[index].skips++;
				this.saveUsers();
				break;
			}
		}
		
		if(this.timerSkip)
			clearTimeout(this.timerSkip);
		
		this.statistics.skips++;
		this.saveStats();
	},
	
	_userFan:function(user){
		var str = this.translate(this.langBot,'USER_FAN');
		str = str.replace('%username%',user.username);
		this._sendChat(str);
	},
	
	_userAdvance:function(obj){
		if (obj == null) return;
		
		this.negative = false;
		if(this.timerSkip)
			clearTimeout(this.timerSkip);
		
		if(typeof obj.lastPlay != 'undefined' && obj.lastPlay)
		{
			var str = this.translate(this.langBot,'PLAYED');
			str = str.replace('%username%',obj.lastPlay.dj.username);
			str = str.replace('%trackauthor%',obj.lastPlay.media.author);
			str = str.replace('%tracktitle%',obj.lastPlay.media.title);
			str = str.replace('%woot%',obj.lastPlay.score.positive);
			str = str.replace('%meh%',obj.lastPlay.score.negative);
			str = str.replace('%love%',obj.lastPlay.score.grabs);
			
			this._sendChat(str);
			
			for(var index in this.usersOnline)
			{
				if(typeof this.users[index] != 'undefined')
				{
					if(this.usersOnline[index].curated)
						this.users[index].curates++;
					
					if(this.usersOnline[index].vote != 0)
					{
						if(this.usersOnline[index].vote == 1)
							this.users[index].woots++;
						else
							this.users[index].mehs++;
					}
					
					if(index == obj.lastPlay.dj.id)
						this.users[index].songs += 1;
				}
				
				this.usersOnline[index].curated = false;
				this.usersOnline[index].vote	= 0;
			}
			
			this.saveUsers();
			
			this.statistics.woots 		+= obj.lastPlay.score.positive;
			this.statistics.mehs 		+= obj.lastPlay.score.negative;
			this.statistics.curates 	+= obj.lastPlay.score.grabs;
			this.statistics.songs 		+= 1;
			this.saveStats();
		}
		
		var timeRemain = 0;
		if(obj.media){
		
			this._inHistory(obj.media,obj.dj.username);
			timeRemain = obj.media.duration;
		}
		
		if(timeRemain > 0){
		
			this._tooLong(timeRemain,obj.dj.username);
			
			if(this.timerBot) window.clearTimeout(this.timerBot);
			this.timerBot = window.setTimeout(function(){window.location.reload();},1000*(timeRemain + 30));
		}
		
		this.vote();
	},
	
	_inHistory:function(media,username)
	{
		var history = API.getHistory();
		if(history){
		
			for(var i in history){
			
				if (history[i].media.author == media.author && history[i].media.title == media.title){
				
					var strInHistory = this.translate(this.langBot,'IN_HISTORY');
						strInHistory = strInHistory.replace('%username%',username);
					
					this.negative = true;
					this.meh();
					
					this._sendChat(strInHistory);
					break;
				}
			}
		}
	},
	
	_tooLong:function(timeRemain,username)
	{
		if((timeRemain > this.maximumTrackLength)){
		
			var timeTimeout = (this.lengthBeforeSkip > 0) ? this.lengthBeforeSkip : this.maximumTrackLength;
			var strTooLong = this.translate(this.langBot,'TOO_LONG');
				strTooLong = strTooLong.replace('%username%',username);
				strTooLong = strTooLong.replace('%seconds%',timeTimeout);
			
			this.negative = true;
			this.meh();
			
			this.timerSkip = window.setTimeout(function(){
			
				API.moderateForceSkip();
			},timeTimeout*1000);
			
			var self = this;
			window.setTimeout(function()
			{
				self._sendChat(strTooLong);
			},1000);
		}
	},
	
	_waitListUpdate:function(users){
	
	},
	
	_scoreUpdate:function(obj){
		if (obj == null) return;
		
		this._thinkMeh(obj);
	},
	
	_userVote:function(obj){
		
		if(typeof this.users[obj.user.id] != 'undefined'){
		
			if(typeof this.usersOnline[obj.user.id] == 'undefined'){
			
				var user = API.getUser(obj.user.id);
				this.setOnlineUser(user);
			}
		
			this.usersOnline[obj.user.id].vote = obj.vote;
		}
	},
	
	_chat:function(data){
		
		var lang = this.getPDJUser(data.uid,'language');
		
		var str = '';
		switch(data.message)
		{
			case this.translateCommand('en','HELP',true):
			case this.translateCommand(lang,'HELP',true):
				str = this.translate(lang,'HELP');
				break;
			case this.translateCommand(lang,'ABOUT',true):
				str = this.translate(lang,'ABOUT');
				str = str.replace('%author%',this.author);
				str = str.replace('%url%',this.url);
				break;
			case this.translateCommand(lang,'VERSION',true):
				str = this.translate(lang,'VERSION');
				str = str.replace('%version%',this.version);
				break;
			case this.translateCommand(lang,'REMOVE_ALL',true):
				var user = API.getUser(data.uid);
				if(Object.keys(user).length === 0)
				{
					str = this.translate(lang,'ERROR');
				}
				else
				{
					if(user.role > API.ROLE.MANAGER)
					{
						str = "TESTING...";
					}
					else
					{
						str = this.translate(lang,'STAFF_REQUIRED');
						str = str.replace('%staff%',this.staffNames[4]);
					}
				}
				break;
			case this.translateCommand(lang,'RELOAD',true):
				var user = API.getUser(data.uid);
				if(Object.keys(user).length === 0)
				{
					str = this.translate(lang,'ERROR');
				}
				else
				{
					if(user.role > 1)
					{
						str = this.translate(this.langBot,'BOT_RELOADING');
						this._sendChat(str);
						this.setVar('reloaded',true);
						setTimeout(function(){window.location.reload(true);},300);
						return;
					}
					else
					{
						str = this.translate(lang,'STAFF_REQUIRED');
						str = str.replace('%staff%',this.staffNames[2]);
					}
				}
				break;
			case this.translateCommand(lang,'JOIN',true):
				var user = API.getUser(data.uid);
				if(Object.keys(user).length === 0)
				{
					str = this.translate(lang,'ERROR');
				}
				else
				{
					if(user.role > 1){
					
						str = this.translate(this.langBot,'BOT_JOINED_DJS');
						API.djJoin();
					}
					else {
					
						str = this.translate(lang,'STAFF_REQUIRED');
						str = str.replace('%staff%',this.staffNames[2]);
					}
				}
				break;
			case this.translateCommand(lang,'LEAVE',true):
				var user = API.getUser(data.uid);
				if(Object.keys(user).length === 0)
				{
					str = this.translate(lang,'ERROR');
				}
				else
				{
					if(user.role > 0){
					
						str = this.translate(this.langBot,'BOT_LEFT_DJS');
						API.djLeave();
					}
					else {
					
						str = this.translate(lang,'STAFF_REQUIRED');
						str = str.replace('%staff%',this.staffNames[1]);
					}
				}
				break;
			case this.translateCommand(lang,'REGISTER',true):
				var user = API.getUser(data.uid);
				if(Object.keys(user).length === 0)
				{
					str = this.translate(lang,'ERROR');
				}
				else
				{
					if(typeof this.users[data.uid] == 'undefined')
					{
						str = this.translate(lang,'REGISTER');
						
						this.users[data.uid]	= {
											username	:data.un,
											joins		:1,
											woots		:0,
											mehs		:0,
											curates		:0,
											songs		:0,
											skips		:0,
											language	:lang
						};
						
						this.saveUsers();
						
						this.setOnlineUser(user);
						if(user.role == API.ROLE.NONE)
							API.moderateSetRole(data.uid,API.ROLE.DJ);
					}
					else
						str = this.translate(lang,'ALREADY_REGISTERED');
				}	
				break;
			case this.translateCommand(lang,'UNREGISTER',true):
				var user = API.getUser(data.uid);
				if(Object.keys(user).length === 0)
				{
					str = this.translate(lang,'ERROR');
				}
				else
				{
					if(typeof this.users[data.uid] != 'undefined')
					{
						str = this.translate(lang,'UNREGISTER');
					
						delete this.users[data.uid];
						delete this.usersOnline[data.uid];
						
						this.saveUsers();
						
						if(user.role == API.ROLE.DJ)
							API.moderateSetRole(data.uid, API.ROLE.NONE);
					}
					else
						str = this.translate(lang,'NOT_REGISTERED');
				}
				break;
			case this.translateCommand(lang,'STATS',true):
			case this.translateCommand(lang,'STATS',true) + '1':
				str = this.sprintStats(lang,1);
				break;
			case this.translateCommand(lang,'STATS',true) + '2':
				str = this.sprintStats(lang,2);
				break;
			case this.translateCommand(lang,'USERSTATS',true):
			case this.translateCommand(lang,'USERSTATS',true) + '1':
				if(typeof this.users[data.uid] != 'undefined')
					str = this.sprintUserStats(lang,data.uid,null,1);
				else
					str = this.translate(lang,'NOT_REGISTERED');
				break;
			case this.translateCommand(lang,'USERSTATS2',true):
				if(typeof this.users[data.uid] != 'undefined')
					str = this.sprintUserStats(lang,data.uid,null,2);
				else
					str = this.translate(lang,'NOT_REGISTERED');
				break;
			case this.translateCommand(lang,'MANUAL',true):
				str = this.translate(lang,'MANUAL_NOT_ENTERED');
				break;
			case this.translateCommand('en','LANGUAGE',true):
			case this.translateCommand(lang,'LANGUAGE',true):
				str = this.translate(lang,'LANGUAGE_NOT_ENTERED');
				break;
			case this.translateCommand(lang,'LANGUAGE_LIST',true):
				str = '@%username% ' + this.sprintLanguages(lang,1);
				break;
			case this.translateCommand('en','COMMANDS',true):
			case this.translateCommand(lang,'COMMANDS',true):
				str = '@%username% ' + this.sprintCommands(lang,1);
				break;
			case this.translateCommand(lang,'TRANSLATOR',true):
				str = this.translate(lang,'LANGUAGE_TRANSLATED_BY');
				str = str.replace('%translator%', this.translator[lang]);
				break;
			default:
			
				var commandFound = false, index = -1, index2 = -1, needle, needle2;
				
				needle = this.translateCommand(lang,'USERSTATS',true) + ' ';
				needle2 = this.translateCommand(lang,'USERSTATS2',true) + ' ';
				
				index = data.message.indexOf(needle);
				index2 = data.message.indexOf(needle2);
				
				if(index === 0 || index2 === 0)
				{
					var page = 1;
					commandFound = true;
					if(index2 === 0)
					{
						page = 2;
						needle 	= needle2;
						index 	= index2;
					}
					
					var foundIndex = '', foundName = '',name = data.message.substr(needle.length);
					
					if(name == data.un)
					{
						str = this.sprintUserStats(lang,data.uid,null,page);
					}
					else
					{
					
						for(var index in this.usersOnline)
						{
							if(typeof this.users[index] != 'undefined')
							{
								if(this.usersOnline[index].username == name)
								{
									foundIndex = index;
									foundName = this.usersOnline[index].username;
									break;
								}
							}
						}
						
						if(foundIndex == ''){
						
							for(var index in this.users)
							{
								if(this.users[index].username == name)
								{
									foundIndex = index;
									foundName = this.users[index].username;
									break;
								}
							}
						}
						
						if(foundIndex == ''){
						
							str = this.translate(lang,'USER_NOT_FOUND');
							str = str.replace('%target%', name);
						}
						else
							str = this.sprintUserStats(lang,foundIndex,foundName,page);
					}
				}
				
				needle = this.translateCommand(lang,'LANGUAGE',true) + ' ';
				index = data.message.indexOf(needle);
				
				needle2 = this.translateCommand('en','LANGUAGE',true) + ' ';
				index2 = data.message.indexOf(needle2);
				
				if((index === 0 || index2 === 0) && commandFound === false)
				{
					if(index2 === 0)
					{
						needle 	= needle2;
						index 	= index2;
					}
				
					commandFound = true;
					var code = data.message.substr(needle.length);
					if(typeof this.lang[code] == "undefined")
						str = this.translate(lang,'LANGUAGE_NOT_FOUND');
					else
					{
						if(typeof this.usersPlugDJ[data.uid] != 'undefined')
							this.usersPlugDJ[data.uid].language = code;
						
						if(typeof this.users[data.uid] != 'undefined')
						{
							this.users[data.uid].language = code;
							this.saveUsers();
						}
						
						str = this.translate(code,'LANGUAGE_CHANGED');
						str = str.replace('%code%', code);
					}
				}
				
				needle = this.translateCommand(lang,'LANGUAGE_LIST',true) + ' ';
				index = data.message.indexOf(needle);
				if(index === 0 && commandFound === false)
				{
					commandFound = true;
					var page = data.message.substr(needle.length);
					page = parseInt(page);
					page = this.setNumber(page);
					
					str = '@%username% ' + this.sprintLanguages(lang,page);
				}
				
				needle = this.translateCommand(lang,'STATS',true) + ' ';
				index = data.message.indexOf(needle);
				if(index === 0 && commandFound === false)
				{
					commandFound = true;
					var page = data.message.substr(needle.length);
					page = parseInt(page);
					page = this.setNumber(page);
					
					str = this.sprintStats(lang,page);
				}
				
				needle = this.translateCommand(lang,'COMMANDS',true) + ' ';
				index = data.message.indexOf(needle);
				if(index === 0 && commandFound === false)
				{
					commandFound = true;
					var page = data.message.substr(needle.length);
					page = parseInt(page);
					page = this.setNumber(page);
					
					str = '@%username% ' + this.sprintCommands(lang,page);
				}
				
				needle = this.translateCommand(lang,'MANUAL',true) + ' ';
				index = data.message.indexOf(needle);
				if(index === 0 && commandFound === false)
				{
					commandFound = true;
					
					var commands = this.getCommands(lang);
					var findCommand = data.message.substr(needle.length);
					var manualFound = false;
					for(index in commands)
					{
						if(commands[index] == findCommand)
						{
							str = '@%username% ' + this.translateManual(lang,index);
							manualFound = true;
							break;
						}
					}
					
					if(!manualFound)
					{
						str = this.translate(lang,'MANUAL_NOT_FOUND');
					}
				}
				
				if(typeof this.bot.username == 'undefined')
				{
					this.setBot();
				}
				
				if(this.chatter != null)
				{
					needle = '@' + this.bot.username + ' ';
					index = data.message.indexOf(needle);
					if(commandFound === false && index === 0){
						
						if(data.un != this.bot.username && data.un != this.chatter.username){
						
							if(this.chatter.timer)
								window.clearTimeout(this.chatter.timer);
							
							var token 	= encodeURIComponent(this.chatter.token);
							var message = data.message.substr(needle.length);
							if(message.length > 0){
								
								this.chatter.username = data.un;
								
								var self = this;
								this.chatter.timer = window.setTimeout(function()
								{
									var url = self.chatter.url;
									url = url.replace('%text%',encodeURIComponent(message));
									url = url.replace('%key%',token);
									
									self.chatter.get(url);
									
								},300);
							}
						}
					}
				}
				
			//CASE END
		}
		
		if(str !== '')
		{
			str = str.replace('%username%',data.un);
			this._sendChat(str);
		}
	}
};

var suBot = new SoundUniverseBot(SuPlugDJRoom);
function ___LoadBot(bot)
{
	setInterval(function(){
	
		var url = window.location.href;
		if(url.indexOf(SuPlugDJRoomSlug) == -1)
		{
			window.location = "https://plug.dj/" + SuPlugDJRoomSlug;
		}
	
	},60*1000);
	
	if(window.location.href.indexOf(SuPlugDJRoomSlug) != -1 && bot.init())
	{
		var iTries = 0;
		var bot = bot;
		
		var findAPI = window.setInterval(function(){
		
			if(typeof API != 'undefined'){
			
				var chatter = new SoundUniverseBotChatter(bot,SuPlugBotDomain,SuPlugBotToken);
				bot.load(chatter);
				
				API.on(API.USER_JOIN, 			function(user)	{bot._userJoin(user);});
				API.on(API.USER_LEAVE, 			function(user)	{bot._userLeave(user);});
				API.on(API.USER_SKIP, 			function(user)	{bot._userSkip(user);});
				//API.on(API.USER_FAN, 			function(user)	{bot._userFan(user);});
				API.on(API.GRAB_UPDATE,			function(obj)	{bot._userCurate(obj);});
				API.on(API.ADVANCE, 			function(obj)	{bot._userAdvance(obj);});
				//API.on(API.WAIT_LIST_UPDATE, 	function(users) {bot._waitListUpdate(users);}); 
				API.on(API.VOTE_UPDATE, 		function(obj)	{bot._userVote(obj);});
				API.on(API.ROOM_SCORE_UPDATE, 	function(obj)	{bot._scoreUpdate(obj);});
				API.on(API.CHAT, 				function(data)	{bot._chat(data);});
				
				window.clearInterval(findAPI);
			}
			else
			{
				if(iTries > 59)
					window.location.reload();
			}
			
			iTries++;
		
		},1000);
	}
}

___LoadBot(suBot);

