	
function SoundUniverseBotLang()
{
	this.lang['en'] 		= {};
	this.lang['sv'] 		= {};
	this.lang['es'] 		= {};
	
	this.commandNames['en'] 	= {};
	this.commandNames['sv'] 	= {};
	this.commandNames['es'] 	= {};
	
	this.commandManuals['en'] 	= {};
	this.commandManuals['sv'] 	= {};
	this.commandManuals['es'] 	= {};
	
	this.autoMessages['en'] 	= [];
	this.autoMessages['sv'] 	= [];
	this.autoMessages['es'] 	= [];
	
	this.staffNames[0] = 'None';
	this.staffNames[1] = 'Resident DJ';
	this.staffNames[2] = 'Bouncer';
	this.staffNames[3] = 'Manager';
	this.staffNames[4] = 'Co-Host';
	this.staffNames[5] = 'Host';
	this.staffNames[6] = 'Ambassador';
	this.staffNames[7] = 'Admin';

	//ENGLISH
	//------------------------------------------------------------------------------------------------------
	
	this.translator['en'] = 'Josh aka XzaR';

	this.commandNames['en']['COMMANDS'] = 'commands';
	this.commandNames['en']['HELP'] = 'help';
	this.commandNames['en']['ABOUT'] = 'about';
	this.commandNames['en']['STATS'] = 'stats';
	this.commandNames['en']['REGISTER'] = 'register';
	this.commandNames['en']['UNREGISTER'] = 'unregister';
	this.commandNames['en']['LANGUAGE'] = 'lang';
	this.commandNames['en']['VERSION'] = 'version';
	this.commandNames['en']['JOIN'] = 'join';
	this.commandNames['en']['LEAVE'] = 'leave';
	this.commandNames['en']['MANUAL'] = 'man';
	this.commandNames['en']['USERSTATS'] = 'userstats';
	this.commandNames['en']['USERSTATS2'] = 'userstats2';
	this.commandNames['en']['LANGUAGE_LIST'] = 'langlist';
	this.commandNames['en']['TRANSLATOR'] = 'translator';
	this.commandNames['en']['RELOAD'] = 'reload';
	this.commandNames['en']['REMOVE_ALL'] = 'removeall';
	
	this.commandManuals['en']['COMMANDS'] = 'Commands is a list of commands available. (Optional Argument: [n])';
	this.commandManuals['en']['HELP'] = 'Help will display useful information.';
	this.commandManuals['en']['ABOUT'] = 'About will display information about the bot.';
	this.commandManuals['en']['STATS'] = 'The statistics command shows the statistics for the room. (Optional Argument: [n])';
	this.commandManuals['en']['REGISTER'] = 'The register command gives you additional features such as userstats.';
	this.commandManuals['en']['UNREGISTER'] = 'The unregister command remove yourself as registered, you will lose all your stats.';
	this.commandManuals['en']['LANGUAGE'] = 'The language command let you select language with two-letter code. (Argument: <s>)';
	this.commandManuals['en']['VERSION'] = 'The version command will display the version of bot.';
	this.commandManuals['en']['JOIN'] = 'The join command makes the bot join the dj table.';
	this.commandManuals['en']['LEAVE'] = 'The leave command makes the bot leave the dj table.';
	this.commandManuals['en']['MANUAL'] = 'The manual command shows information for a specific command. (Argument: <s>)';
	this.commandManuals['en']['USERSTATS'] = 'The userstats command shows statistics for a registered user in the room. (Optional Argument: [s])';
	this.commandManuals['en']['USERSTATS2'] = this.commandManuals['en']['USERSTATS'];
	this.commandManuals['en']['LANGUAGE_LIST'] = 'The language list command shows all available languages. (Optional Argument: [n])';
	this.commandManuals['en']['TRANSLATOR'] = 'The translator command show the author of the translation.';
	this.commandManuals['en']['RELOAD'] = 'Refresh the browser of the bot.';
	this.commandManuals['en']['REMOVE_ALL'] = 'Remove all registered users.';
	
	this.autoMessages['en'][0] = '/me : This Room is using Sound Universe Bot \
								please type !' + this.commandNames['en']['HELP'] + '. for more \
								information. If you have any question type @%botname% <question>.';
								
	this.autoMessages['en'][1] = '/me : This Room is using custom theme and background. \
								To see them please install plug³, https://plugcubed.net/';
								
	this.autoMessages['en'][2] = '/me : Do you want userstats? Type !' + this.commandNames['en']['REGISTER'] + '. \
								You will also become a Resident DJ.';
	
	//TEXT & LABELS
	//----------------------------------------------------------
	
	this.lang['en']['C_VERSION'] 	= 'Sound Universe Bot v%version%.';
	this.lang['en']['C_LOADED'] 	= 'Sound Universe Bot LOADED.';
	
	this.lang['en']['WELCOME'] 		= '@%username% Welcome to %room% for more information \
										type !' + this.commandNames['en']['HELP'] + '. If you have \
										any question type @%botname% <question>.';
	
	this.lang['en']['HELP']			= '@%username% Type !' + this.commandNames['en']['COMMANDS'] + ' to show \
										a list with commands, !' + this.commandNames['en']['MANUAL'] + ' <s> for the command manual.';
	
	this.lang['en']['CURATED'] 		= '/me : %username% love this song, %trackauthor% - %tracktitle%. :heart:';
	this.lang['en']['VERSION'] 		= '@%username% Sound Universe Bot v%version%.';
	
	this.lang['en']['JOINED'] 		= '/me %username% have joined.';
	this.lang['en']['LEFT'] 		= '/me %username% have left.';
	
	this.lang['en']['MANUAL_NOT_ENTERED'] 	= '@%username% To show a manual you must also enter a command name.';
	this.lang['en']['MANUAL_NOT_FOUND'] 	= '@%username% The manual could not be found.';
	
	this.lang['en']['ABOUT'] 			= '@%username% This bot was made for Sound Universe room created by %author%. %url%';
	
	this.lang['en']['STATS'] 			=	'@%username% Room Statistics - Woots: %woots%, Mehs: %mehs%, Votes: %votes%, \
											Grabs: %curates%. !' + this.commandNames['en']['STATS'] + ' 2 for more.';
	
	this.lang['en']['STATS2'] 			=	'@%username% Room Statistics 2 - Songs: %songs%, Skips: %skips%, \
											Joins: %joins%, Registered Users: %users%.';
	
	this.lang['en']['USERSTATS_SELF'] 		= "Users Statistics";
	this.lang['en']['USERSTATS_TARGET'] 	= "%target%'s Statistics";
	
	this.lang['en']['USERSTATS'] 		=	'@%username% (%target% #1) - \
											Woots: %woots%, Mehs: %mehs%, Grabs: %curates%. \
											!' + this.commandNames['en']['USERSTATS2'] + ' for more.';
											
	this.lang['en']['USERSTATS2'] 		=	'@%username% (%target% #2) - Joins: %joins%, \
											Votes: %votes% Songs: %songs%, Skips: %skips%.';
	
	this.lang['en']['PLAYED'] 		= 	'/me : %username% played the song, %trackauthor% - %tracktitle%. :musical_note: \
										|| Woots: %woot%:green_heart: Mehs: %meh%:broken_heart: Loves: %love%:purple_heart:';
	
	this.lang['en']['TOO_LONG']					= '/me @%username% the song is too long. Skipped in %seconds% seconds.';
	this.lang['en']['IN_HISTORY']				= '/me @%username% the song has already been played, please check the history.';
	
	this.lang['en']['LANGUAGE_TRANSLATED_BY']	= '@%username% The language was translated by %translator%.';
	this.lang['en']['LANGUAGE_NOT_ENTERED'] 	= '@%username% To set language you must also enter a two-letter code.';
	this.lang['en']['LANGUAGE_CHANGED']				= '@%username% The language is set to %code%.';
	this.lang['en']['LANGUAGE_NOT_FOUND']		= '@%username% The language could not be found. \
													Type !' + this.commandNames['en']['LANGUAGE_LIST'] + ' for all languages.';
	
	this.lang['en']['ALREADY_REGISTERED']		= '@%username% You are already registered.';
	this.lang['en']['NOT_REGISTERED']			= '@%username% You are not registered. Type !' + this.commandNames['en']['REGISTER'] + '.';
	this.lang['en']['REGISTER'] 				= '@%username% You have successfully registered.';
	this.lang['en']['UNREGISTER'] 				= '@%username% You have successfully unregistered.';
	this.lang['en']['USER_NOT_FOUND']			= '@%username% The the registered user %target% can\'t be found.';
	this.lang['en']['USER_FAN']					= '@%username% :kiss:';
	
	this.lang['en']['STAFF_REQUIRED']			= '@%username% You must be in the staff to use this command. (%staff%)';
	
	this.lang['en']['BOT_JOINED_DJS']			= '/me Time to show who the djs is! :sparkles:';
	this.lang['en']['BOT_LEFT_DJS']				= '/me What! Are you kicking me out? Leaving... :droplet:';
	
	this.lang['en']['ERROR']				= '@%username% Something went wrong! :(';
	this.lang['en']['BOT_RELOADING'] 		= '/me I will be back soon...';
	this.lang['en']['BOT_RELOADED'] 		= '/me I am back!';
	
	
	//Swedish
	//------------------------------------------------------------------------------------------------------
	
	this.translator['sv'] = 'Mr. KING aka XzaR';

	this.commandNames['sv']['COMMANDS'] = 'kommandon';
	this.commandNames['sv']['HELP'] = 'hjälp';
	this.commandNames['sv']['ABOUT'] = 'om';
	this.commandNames['sv']['STATS'] = 'stats';
	this.commandNames['sv']['REGISTER'] = 'registrera';
	this.commandNames['sv']['UNREGISTER'] = 'avregistrera';
	this.commandNames['sv']['LANGUAGE'] = 'språk';
	this.commandNames['sv']['VERSION'] = 'version';
	this.commandNames['sv']['JOIN'] = 'anslut';
	this.commandNames['sv']['LEAVE'] = 'lämna';
	this.commandNames['sv']['MANUAL'] = 'man';
	this.commandNames['sv']['USERSTATS'] = 'användarstats';
	this.commandNames['sv']['USERSTATS2'] = 'användarstats2';
	this.commandNames['sv']['LANGUAGE_LIST'] = 'språklista';
	this.commandNames['sv']['TRANSLATOR'] = 'översättare';
	this.commandNames['sv']['RELOAD'] = 'reload';
	
	this.commandManuals['sv']['COMMANDS'] = 'Kommandon är en lista med alla kommandonamn som finns. (Valfritt Argument: [n])';
	this.commandManuals['sv']['HELP'] = 'Hjälp visar användbar information.';
	this.commandManuals['sv']['ABOUT'] = 'Om visar information angående Plug.Su Bot.';
	this.commandManuals['sv']['STATS'] = 'Kommandot statistik visar rummet\'s statistik. (Valfritt Argument: [n])';
	this.commandManuals['sv']['REGISTER'] = 'Kommandot registrera ger dig ytterligare funktioner så som användarstatistik.';
	this.commandManuals['sv']['UNREGISTER'] = 'Kommandot avregistrera ta bort dig själv som medlem, du kommer att förlora all din statistik.';
	this.commandManuals['sv']['LANGUAGE'] = 'Välj nytt språk med kommandot språk, två-bokstavs kod behövs. (Argument: <s>)';
	this.commandManuals['sv']['VERSION'] = 'Kommandot version visar vilken variant av bot det är.';
	this.commandManuals['sv']['JOIN'] = 'Kommandot anslut gör att botten väntar på sin tur att spela på djbåset.';
	this.commandManuals['sv']['LEAVE'] = 'Kommandot lämna gör att bottem lämnar sin plats på djbåset.';
	this.commandManuals['sv']['MANUAL'] = 'Kommandot manual visas information om ett visst kommando. (Argument: <s>)';
	this.commandManuals['sv']['USERSTATS'] = 'Kommandot användarstats visar statistik för en registrerad användare i rummet. (Valfritt Argument: [s])';
	this.commandManuals['sv']['USERSTATS2'] = this.commandManuals['sv']['USERSTATS'];
	this.commandManuals['sv']['LANGUAGE_LIST'] = 'Kommandot språklista visar alla tillgängliga språk. (Valfritt Argument: [n])';
	this.commandManuals['sv']['TRANSLATOR'] = 'Kommandot översättare visar författaren av tolkningen.';
	this.commandManuals['sv']['RELOAD'] = 'Startar om sidan i webbläsaren för botten.';
	
	this.autoMessages['sv'][0] 	= '/me : Det här rummet använder Sound Universe Bot \
									var snäll skriv !' + this.commandNames['sv']['HELP'] + '. för mer \
									information. Om du har någon fråga skriv @%botname% <fråga på Engelska>.';
									
	this.autoMessages['sv'][1] = '/me : Det här rummet använder eget tema. \
								För att se den var snäll och installera plug³, https://plugcubed.net/';
								
	this.autoMessages['sv'][2] = '/me : Vill du användarstatistik? Skriv !' + this.commandNames['sv']['REGISTER'] + '. \
								Du kommer också att bli en Resident DJ.';
	
	//TEXT & LABELS
	//----------------------------------------------------------
	
	this.lang['sv']['C_VERSION'] 	= 'Sound Universe Bot v%version%.';
	this.lang['sv']['C_LOADED'] 	= 'Sound Universe Bot LADDAD.';
	
	this.lang['sv']['WELCOME'] 		= '@%username% Välkommen till %room% för mer information \
										skriv !' + this.commandNames['sv']['HELP'] + '. om du har \
										någon fråga skriv @%botname% <fråga på Engelska>.';
	
	this.lang['sv']['HELP']			= '@%username% Skriv !' + this.commandNames['sv']['COMMANDS'] + ' för att visa \
										en lista med kommandon, !' + this.commandNames['sv']['MANUAL'] + ' <s> för en specifik manual.';
	
	this.lang['sv']['CURATED'] 		= '/me : %username% älskar den här song, %trackauthor% - %tracktitle%. :heart:';
	this.lang['sv']['VERSION'] 		= '@%username% Sound Universe Bot v%version%.';
	
	this.lang['sv']['JOINED'] 		= '/me %username% har anslutit.';
	this.lang['sv']['LEFT'] 		= '/me %username% har lämnat.';
	
	this.lang['sv']['MANUAL_NOT_ENTERED'] 	= '@%username% För att visa en manual måste du även skriva med ett kommandonamn.';
	this.lang['sv']['MANUAL_NOT_FOUND'] 	= '@%username% Manualen finns inte.';
	
	this.lang['sv']['ABOUT'] 			= '@%username% Den här boten var gjord för Sound Universe rummet och är skapad av %author%. %url%';
	
	this.lang['sv']['STATS'] 			=	'@%username% Rumstatistik - Woots: %woots%, Mehs: %mehs%, Röster: %votes%, \
											Älskar: %curates%. !' + this.commandNames['sv']['STATS'] + ' 2 för mer.';
	
	this.lang['sv']['STATS2'] 			=	'@%username% Rumstatistik 2 - Låtar: %songs%, Överhoppade: %skips%, \
											Anslutit: %joins%, Medlemar: %users%.';

	this.lang['sv']['USERSTATS_SELF'] 		= "Användarstatistik";
	this.lang['sv']['USERSTATS_TARGET'] 	= "%target%'s Statistik";
											
	this.lang['sv']['USERSTATS'] 		=	'@%username% (%target% #1) - \
											Woots: %woots%, Mehs: %mehs%, Älskar: %curates%. \
											!' + this.commandNames['sv']['USERSTATS2'] + ' för mer.';
											
	this.lang['sv']['USERSTATS2'] 		=	'@%username% (%target% #2) - Anslutit: %joins%, \
											Röster: %votes% Låtar: %songs%, Överhoppade: %skips%.';
	
	this.lang['sv']['PLAYED'] 		= 	'/me : %username% spelade låten, %trackauthor% - %tracktitle%. :musical_note: \
										|| Woots: %woot%:green_heart: Mehs: %meh%:broken_heart: Älskar: %love%:purple_heart:';
	
	this.lang['sv']['TOO_LONG']					= '/me @%username% låten är för lång. Hoppar över om %seconds% sekunder.';
	this.lang['sv']['IN_HISTORY']				= '/me @%username% sågen har redan spelats upp, var snäll och kolla upp dem senast uppspelade låtarna.';
	
	this.lang['sv']['LANGUAGE_TRANSLATED_BY']	= '@%username% Språket var översatt av %translator%.';
	this.lang['sv']['LANGUAGE_NOT_ENTERED'] 	= '@%username% För att välja språk måste du fylla i två-bokstavs språkkod.';
	this.lang['sv']['LANGUAGE_CHANGED']			= '@%username% Språket är satt till %code%.';
	this.lang['sv']['LANGUAGE_NOT_FOUND']		= '@%username% Språket kunde inte hittas. \
													Skriv !' + this.commandNames['sv']['LANGUAGE_LIST'] + ' för att hitta alla som finns.';
	
	this.lang['sv']['ALREADY_REGISTERED']		= '@%username% Du är redan medlem.';
	this.lang['sv']['NOT_REGISTERED']			= '@%username% Du är inte medlem. Skriv !' + this.commandNames['sv']['REGISTER'] + '.';
	this.lang['sv']['REGISTER'] 				= '@%username% Grattis! Du har blivit registerad.';
	this.lang['sv']['UNREGISTER'] 				= '@%username% Du har nu avregisterat dig.';
	this.lang['sv']['USER_NOT_FOUND']			= '@%username% Den registerade användaren %target% kan inte hittas.';
	this.lang['sv']['USER_FAN']					= '@%username% :kiss:';
	
	this.lang['sv']['STAFF_REQUIRED']			= '@%username% Du har igen tillträde för att använda kommandot. (%staff%)';
	
	this.lang['sv']['BOT_JOINED_DJS']			= '/me Wohoo! nu ska jag visa vem som är DJ. :sparkles:';
	this.lang['sv']['BOT_LEFT_DJS']				= '/me Va! Var jag inte tillräckligt bra? Lämnar... :droplet:';
	
	this.lang['sv']['ERROR']				= '@%username% Något gick fel! :(';
	this.lang['sv']['BOT_RELOADING'] 		= '/me Jag är snart tillbaka...';
	this.lang['sv']['BOT_RELOADED'] 		= '/me Jag är nu tillbaka!';
	
	//Spanish
	//------------------------------------------------------------------------------------------------------
	
	this.translator['es'] = 'carlosvs11 o TeraFrost';

	this.commandNames['es']['COMMANDS'] = 'commands';
	this.commandNames['es']['HELP'] = 'help';
	this.commandNames['es']['ABOUT'] = 'about';
	this.commandNames['es']['STATS'] = 'stats';
	this.commandNames['es']['REGISTER'] = 'register';
	this.commandNames['es']['UNREGISTER'] = 'unregister';
	this.commandNames['es']['LANGUAGE'] = 'lang';
	this.commandNames['es']['VERSION'] = 'version';
	this.commandNames['es']['JOIN'] = 'join';
	this.commandNames['es']['LEAVE'] = 'leave';
	this.commandNames['es']['MANUAL'] = 'man';
	this.commandNames['es']['USERSTATS'] = 'userstats';
	this.commandNames['es']['USERSTATS2'] = 'userstats2';
	this.commandNames['es']['LANGUAGE_LIST'] = 'langlist';
	this.commandNames['es']['TRANSLATOR'] = 'translator';
	this.commandNames['es']['RELOAD'] = 'reload';
   
	this.commandManuals['es']['COMMANDS'] = 'Enseña una lista de comandos. (Optional Argument: [n])';
	this.commandManuals['es']['HELP'] = '"Help" Enseña algo de ayuda.';
	this.commandManuals['es']['ABOUT'] = '"About" Enseña informacion del bot.';
	this.commandManuals['es']['STATS'] = 'El comando "Stats" muestra estadisticas de la sala. (Optional Argument: [n])';
	this.commandManuals['es']['REGISTER'] = 'El comando "Register" te dara ventajas como las estadisticas de usuario.';
	this.commandManuals['es']['UNREGISTER'] = 'El comando "Unregister" eliminara tu estado de registrado y perderas tus estadisticas.';
	this.commandManuals['es']['LANGUAGE'] = 'El comando "Language" te permite cambiar el idioma (Argument: <s>)';
	this.commandManuals['es']['VERSION'] = 'El comando "Version" muestra la version del bot.';
	this.commandManuals['es']['JOIN'] = 'El comando "Join" hace que el bot se una a la lista de espera.';
	this.commandManuals['es']['LEAVE'] = 'El comando "Leave" hace que el bot deje la lista de espera/cabina de DJ.';
	this.commandManuals['es']['MANUAL'] = 'El comando "Man" enseña informacion de un comando en particular. (Argument: <s>)';
	this.commandManuals['es']['USERSTATS'] = 'El comando de "Userstats" muestra las estadisticas de usuario. (Optional Argument: [s])';
	this.commandManuals['es']['USERSTATS2'] = this.commandManuals['es']['USERSTATS'];
	this.commandManuals['es']['LANGUAGE_LIST'] = 'Este comando lista los idiomas disponibles. (Optional Argument: [n])';
	this.commandManuals['es']['TRANSLATOR'] = 'El comando "translator" muestra quien creo la traduccion.';
	this.commandManuals['es']['RELOAD'] = 'Recarga el bot.';
	
	this.autoMessages['es'][0] = '/me : Esta sala esta usando el bot SU.Bot \
									Porfavor escribe  !' + this.commandNames['es']['HELP'] + '. para mas \
									informacion. Si tienes alguna duda escribe @%botname% <pregunta> (en ingles).';
									
	this.autoMessages['es'][1] = '/me : This Room is using custom theme and background. \
								To see them please install plug³, https://plugcubed.net/';
								
	this.autoMessages['es'][2] = '/me : Do you want userstats? Type !' + this.commandNames['es']['REGISTER'] + '. \
								You will also become a Resident DJ.';
									
	//TEXT & LABELS
	//----------------------------------------------------------
   
	this.lang['es']['C_VERSION']    = 'Sound Universe Bot v%version%.';
	this.lang['es']['C_LOADED']     = 'Sound Universe Bot CARGADO.';
   
	this.lang['es']['WELCOME']              = '@%username% Bienvenido a %room% \
												escribe !' + this.commandNames['es']['HELP'] + '. Si necesitas ayuda\
												Si tienes alguna duda escribe @%botname% <pregunta> (en ingles).';
   
   
	this.lang['es']['HELP']                 = '@%username% Escribe !' + this.commandNames['es']['COMMANDS'] + ' para mostrar \
												una lista de comandos, !' + this.commandNames['es']['MANUAL'] + ' <s> para el manual del comando.';
   
	this.lang['es']['CURATED']              = '/me : %username% ama esta cancion, %trackauthor% - %tracktitle%. :heart:';
	this.lang['es']['VERSION']              = '@%username% Sound Universe Bot v%version%.';
   
	this.lang['es']['JOINED']               = '/me %username% se ha unido.';
	this.lang['es']['LEFT']                 = '/me %username% ha abandonado la sala.';
   
	this.lang['es']['MANUAL_NOT_ENTERED']   = '@%username% Para mostrar un manual debes poner el nombre del manual.';
	this.lang['es']['MANUAL_NOT_FOUND']     = '@%username% El manual no se pudo encontrar.';
   
	this.lang['es']['ABOUT'] 	= '@%username% Este bot fue creado para la sala Sound Universe por %author%. %url%';
   
	this.lang['es']['STATS'] 	=       '@%username% Estadísticas de la sala - Woots: %woots%, Mehs: %mehs%, Votes: %votes%, \
											Grabs: %curates%. !' + this.commandNames['es']['STATS'] + ' 2 para más información.';
   
	this.lang['es']['STATS2'] 	=       '@%username% Estadísticas de la sala 2 - Canciones: %songs%, Skips: %skips%, \
											Joins: %joins%, Usuarios registrados: %users%.';
   
	this.lang['es']['USERSTATS_SELF'] 		= "Estadisticas de usuario";
	this.lang['es']['USERSTATS_TARGET'] 	= "Estadisticas de %target%";
   
	this.lang['es']['USERSTATS']        =       '@%username% (%target% #1) - \
													Woots: %woots%, Mehs: %mehs%, Grabs: %curates%. \
													!' + this.commandNames['es']['USERSTATS2'] + ' para más información.';
																				   
	this.lang['es']['USERSTATS2']  		=       '@%username% (%target% #1) - Joins: %joins%, \
													Votos: %votes% Canciones: %songs%, Skips: %skips%.';
   
	this.lang['es']['PLAYED'] 			=       '/me : %username% puso la cancion, %trackauthor% - %tracktitle%. :musical_note: \
													|| Woots: %woot%:green_heart: Mehs: %meh%:broken_heart: Grabs: %love%:purple_heart:';
   
	this.lang['es']['TOO_LONG'] 	= '/me ¡@%username% la cancion es demasiado larga!. Saltado en %seconds% seconds.';
	this.lang['es']['IN_HISTORY']   = '/me @%username% esta cancion ya ha sido tocada,comprueba el historial.';
   
	this.lang['es']['LANGUAGE_TRANSLATED_BY']       = '@%username% La traduccion ha sido creada por  %translator%.';
	this.lang['es']['LANGUAGE_NOT_ENTERED']         = '@%username% Para poner un idioma debes poner el codigo de dos digitos (es,en).';
	this.lang['es']['LANGUAGE_CHANGED']             = '@%username% El idioma ha sido cambiado a %code%.';
	this.lang['es']['LANGUAGE_NOT_FOUND']           = '@%username% El idioma no se pudo encontrar. \
														Escribe!' + this.commandNames['es']['LANGUAGE_LIST'] + ' para ver todos los idiomas.';
   
	this.lang['es']['ALREADY_REGISTERED'] 	= '@%username% Ya estabas registrado.';
	this.lang['es']['NOT_REGISTERED'] 		= '@%username% No estas registrado. Escribe!' + this.commandNames['es']['REGISTER'] + '.';
	this.lang['es']['REGISTER'] 			= '@%username% Te has registrado satisfactoriamente.';
	this.lang['es']['UNREGISTER'] 			= '@%username% Ya no estas registrado.';
	this.lang['es']['USER_NOT_FOUND'] 		= '@%username% El usuario registrado %target% no se pudo encontrar.';
	this.lang['es']['USER_FAN'] 			= '@%username% :kiss:';
   
	this.lang['es']['STAFF_REQUIRED'] 		= '@%username% Debes estar en el equipo para hacer esto. (%staff%)';
   
	this.lang['es']['BOT_JOINED_DJS']       = '/me ¡Hora de tocar! :sparkles:';
	this.lang['es']['BOT_LEFT_DJS']         = '/me ¡¿Que?! ¿Me echas?... :droplet:';
   
	this.lang['es']['ERROR']   			= '@%username% Hubo un error inesperado,prueba otra vez. :(';
	this.lang['es']['BOT_RELOADING'] 	= '/me Volvere pronto...';
	this.lang['es']['BOT_RELOADED'] 	= '/me Ya estoy de vuelta!';
	
}

