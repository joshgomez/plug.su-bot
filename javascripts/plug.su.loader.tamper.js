// ==UserScript==
// @name		Sound Universe Bot Loader for Tampermonkey (Chrome)
// @namespace	http://www.xuniver.se
// @version		2.0
// @description	Simple PlugDJ bot loader.
// @match		https://plug.dj/*
// @copyright	2014+, XzaR
// ==/UserScript==

var SuBotURL = 'https://ENTER-YOUR-DOMAIN/plug.su-bot/javascripts/';

window.addEventListener('DOMContentLoaded',function(e)
{
	setTimeout(function(){
	
		var script;
		
		script = document.createElement('script');
		script.src = SuBotURL + 'plug.su.lang.js';
		document.body.appendChild(script);
		
		script = document.createElement('script');
		script.src = SuBotURL + 'plug.su.js';
		document.body.appendChild(script);
		
	},5000);
});