<?php

	ini_set('display_errors', 'Off');

	header('Access-Control-Allow-Origin: https://plug.dj');
	header('Access-Control-Allow-Methods: GET');

	date_default_timezone_set('Europe/Stockholm');
	$key = 'ENTER_YOUR_TOKEN';

	$lang = [];
	$lang['TXT']['SPAM'] = 'Please don\'t spam.<br />Spammed: %d Total: %d';
	$lang['TXT']['NOT_AJAX'] = 'This page must be requested with AJAX.';
	$lang['TXT']['LOST_THOUGHT'] = 'What... Can you say it again please? Possible errors. :(';
	
	mb_http_output('UTF-8');
	mb_internal_encoding('UTF-8');
	
	session_start();

	$milliseconds = round(microtime(true) * 1000);
	if(empty($_SESSION['spam']))
	{
		$_SESSION['spam'] = array($milliseconds,1,0);
	}
	else
	{
		if(($milliseconds - 200) < $_SESSION['spam'][0])
		{
			$_SESSION['spam'][1] += 1;
			$_SESSION['spam'][2] += 1;
			
			if($_SESSION['spam'][1] > 3)
			{
				sleep(5);
			}

			die(sprintf($lang['TXT']['SPAM'],$_SESSION['spam'][1],$_SESSION['spam'][2]));
		}
		else
		{
			$_SESSION['spam'][1] = 0;
		}
		
		$_SESSION['spam'][0] = $milliseconds;
	}
	
	/*
	
	if(empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		die($lang['TXT']['NOT_AJAX']);
	}
	
	*/
	
	$text = '';
	$token = isset($_GET['token']) ? $_GET['token'] : '';
	if($token === $key)
	{
		$message = isset($_GET['message']) ? $_GET['message'] : '';
		if($message)
		{
			require __DIR__ . '/chatterbotapi.php';
			
			$factory = new ChatterBotFactory();
			if(empty($_SESSION['botSession']))
			{
				$bot = [];
				$botSession = [];
			
				//$bot[0] = $factory->create(ChatterBotType::CLEVERBOT);
				
				$bot[0] = $factory->create(ChatterBotType::JABBERWACKY);
				$botSession[0] = $bot[0]->createSession();
				
				$bot[1] = $factory->create(ChatterBotType::PANDORABOTS, 'b0dafd24ee35a477');
				$botSession[1] = $bot[1]->createSession();
				
				$_SESSION['bot']		= serialize($bot);
				$_SESSION['botSession'] = serialize($botSession);
			}
			else
			{
				$bot 		= unserialize($_SESSION['bot']);
				$botSession = unserialize($_SESSION['botSession']);
			}
		
			$botSession[0] = isset($botSession[0]) && is_object($botSession[0]) ? $botSession[0] : false;
			$botSession[1] = isset($botSession[1]) && is_object($botSession[1]) ? $botSession[1] : false;
			
			if($botSession[0] || $botSession[1])
			{
				$message = htmlspecialchars($message,ENT_QUOTES);
				
				$think = [];
				if($botSession[0])
					$think[] = $botSession[0]->think($message);
					
				if($botSession[1])
					$think[] = $botSession[1]->think($message);
					
				$key = array_rand($think);
				
				$text = $think[$key];
				if($text == '')
				{
					unset($_SESSION['bot']);
					unset($_SESSION['botSession']);
					
					$text = $lang['TXT']['LOST_THOUGHT'];
				}
				
				$text = strip_tags($text,'<a><br>');
				$text = preg_replace('/<a href=\"(.*?)\" target=\"_blank\">(.*?)<\/a>/iu', "\\2", $text);
				
				usleep(500000);
			}
		}
	}
	else
	{
		$text = 'The token key is wrong. Please enter the right key in plug.su.js file.';
	}
	
	print $text;

?>